import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MonPremierComponent } from './mon-premier/mon-premier.component';
import { CombattantComponent } from './combattant/combattant.component';

import { CombattantService } from './services/combattant.service';
import { AuthComponent } from './auth/auth.component';
import { CombattantViewComponent } from './combattant-view/combattant-view.component';
import { AuthService } from './services/auth.service';
import { SingleCombattantComponent } from './single-combattant/single-combattant.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { AuthGuard } from './services/auth-guard.service';
import { EditCombattantComponent } from './edit-combattant/edit-combattant.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserService } from './services/user.service';
import { NewUserComponent } from './new-user/new-user.component';

@NgModule({
    declarations: [
        AppComponent,
        MonPremierComponent,
        CombattantComponent,
        AuthComponent,
        CombattantViewComponent,
        SingleCombattantComponent,
        FourOhFourComponent,
        EditCombattantComponent,
        UserListComponent,
        NewUserComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    providers: [
        CombattantService,
        AuthService,
        AuthGuard,
        UserService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
//on importe les modules et component
