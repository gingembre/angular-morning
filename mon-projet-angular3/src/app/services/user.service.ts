import { User } from '../models/User.model';
import { Subject } from 'rxjs/Subject';

export class UserService {
    private users: User[] = [
        {
            firstName: 'Damien',
            lastName: 'Eychenne',
            email: 'damien.eychenne.m@gmail.com',
            mangaPreference: 'Hoshin',
            hobbies: [
                'coder',
                'le rétro-gaming'
            ]
        }
    ];
    userSubject = new Subject<User[]>();
    
    emitUsers() {
        this.userSubject.next(this.users.slice());
    }
    
    addUser(user: User) {
        this.users.push(user);
        this.emitUsers();
    }
}