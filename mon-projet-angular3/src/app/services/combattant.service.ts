import { Subject } from 'rxjs/Subject';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()

export class CombattantService {
    
    combattantsSubject = new Subject<any[]>();
    
    private combattants = [];
    
    constructor( private httpClient: HttpClient) {}
    
    emitCombattantSubject() {
        this.combattantsSubject.next(this.combattants.slice());
    }
    
    getCombattantById(id: number) {
        const combattant = this.combattants.find(
            (combattantObject) => {
                return combattantObject.id === id;
            }
        );
        return combattant;
    }
    
    switchOnAll() {
        for(let combattant of this.combattants) {
            combattant.status = 'prêt au combat'
        }
        this.emitCombattantSubject();
    }
    
    switchOffAll() {
        for(let combattant of this.combattants) {
            combattant.status = 'blessé';
        }
        this.emitCombattantSubject();
    }
    
    switchOnOne(index: number) {
        this.combattants[index].status = 'prêt au combat';
        this.emitCombattantSubject();
    }
    switchOffOne(index: number) {
        this.combattants[index].status = 'blessé';
        this.emitCombattantSubject();
    }
    addCombattant(name: string, status: string) {
        const combattantObject = {
            id: 0,
            name: '',
            status: ''
        };
        combattantObject.name = name;
        combattantObject.status = status;
        combattantObject.id = this.combattants[(this.combattants.length - 1)].id + 1;
        
        this.combattants.push(combattantObject);
        this.emitCombattantSubject();
    }
    
    saveCombattantsToServer() {
        this.httpClient
            .put('https://http-angular-application.firebaseio.com/combattants.json', this.combattants)
        .subscribe(
            () => {
                console.log('Enregistrement terminé ! ');
            },
            (error) => {
                console.log('Erreur de sauvegarde ! ' + error);
            }
        );
    }
    
    getCombattantsFromServer() {
        this.httpClient
            .get<any[]>('https://http-angular-application.firebaseio.com/combattants.json')
            .subscribe(
                (response) => {
                    this.combattants = response;
                    this.emitCombattantSubject();
                },
                    (error) => {
                        console.log('Erreur de chargement ! ' + error);
                    }
        );
    }
}