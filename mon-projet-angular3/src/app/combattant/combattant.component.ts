import { Component, Input, OnInit } from '@angular/core';
import { CombattantService } from '../services/combattant.service';

@Component({
    selector: 'app-combattant',
    templateUrl: './combattant.component.html',
    styleUrls: ['./combattant.component.scss']
})
export class CombattantComponent implements OnInit {

    @Input() combattantName: string;
    @Input() combattantStatus: string;
    @Input() indexOfCombattant: number;
    @Input() id: number;

    constructor(private combattantService: CombattantService) { }

    ngOnInit() {
    }
    getStatus() {
        return this.combattantStatus;
    }
    getColor() {
        if(this.combattantStatus === 'prêt au combat') {
            return 'green';
        } else if(this.combattantStatus === 'blessé') {
            return 'red';
        }
    }
    onSwitchOn() {
        this.combattantService.switchOnOne(this.indexOfCombattant);
    }
    onSwitchOff() {
        this.combattantService.switchOffOne(this.indexOfCombattant);
    }
}
