export class User {
    
    constructor(
        public firstName: string,
        public lastName: string,
        public email: string,
        public mangaPreference: string,
        public hobbies?: string[]
    ) {}   
}