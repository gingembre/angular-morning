import { Component, OnInit } from '@angular/core';
import { CombattantService } from '../services/combattant.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-combattant-view',
    templateUrl: './combattant-view.component.html',
    styleUrls: ['./combattant-view.component.scss']
})

export class CombattantViewComponent implements OnInit {
    isAuth = false;
    //ici apparation de la date
    lastUpdate = new Promise((resolve, reject) => { 
        const date = new Date();
        setTimeout(
            () => {
                resolve(date);
            },   2000
        );
    });
    //ici les combattants
    combattants: any[];
    combattantSubscription: Subscription;
  
    constructor(private combattantService: CombattantService) {}

    ngOnInit() {
        this.combattantSubscription = this.combattantService.combattantsSubject.subscribe(
            (combattants: any[]) => {
                this.combattants = combattants;
            }
        );
        this.combattantService.emitCombattantSubject();
    }

    onRestaurer(){
        this.combattantService.switchOnAll();
    }
    onBlesse() {
        if(confirm('Etes-vous sûr de faire combattre tous vos combattants ?')) {
            this.combattantService.switchOffAll();
        } else {
          return null;
        }
    }
    
    onSave() {
        this.combattantService.saveCombattantsToServer();
    }
    onFetch() {
        this.combattantService.getCombattantsFromServer();
    }
}
