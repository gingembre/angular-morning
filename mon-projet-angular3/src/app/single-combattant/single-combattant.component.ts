import { Component, OnInit } from '@angular/core';
import { CombattantService } from '../services/combattant.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-combattant',
  templateUrl: './single-combattant.component.html',
  styleUrls: ['./single-combattant.component.scss']
})
export class SingleCombattantComponent implements OnInit {
    
    name: string = 'Combattant';
    status: string = 'Statut';

  constructor(private combattantService: CombattantService, 
              private route: ActivatedRoute) { }

  ngOnInit() {
      const id = this.route.snapshot.params['id'];
      this.name = this.combattantService.getCombattantById(+id).name;
      this.status = this.combattantService.getCombattantById(+id).status;
  }
}
