import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CombattantService } from '../services/combattant.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-combattant',
  templateUrl: './edit-combattant.component.html',
  styleUrls: ['./edit-combattant.component.scss']
})
export class EditCombattantComponent implements OnInit {
    
    defaultOnOff = 'blessé';

  constructor( private combattantService: CombattantService,
               private router: Router) { }

  ngOnInit() {
  }

    onSubmit(form: NgForm) {
        const name = form.value['name'];
        const status = form.value['status'];
        this.combattantService.addCombattant(name, status);
        this.router.navigate(['combattants']);
    }
}
