import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CombattantService } from './services/combattant.service';
import { AuthComponent } from './auth/auth.component';
import { CombattantViewComponent } from './combattant-view/combattant-view.component';
import { SingleCombattantComponent } from './single-combattant/single-combattant.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { AuthGuard } from './services/auth-guard.service';
import { EditCombattantComponent } from './edit-combattant/edit-combattant.component';
import { UserService } from './services/user.service';
import { UserListComponent } from './user-list/user-list.component';
import { NewUserComponent } from './new-user/new-user.component';

const routes: Routes = [
    { path: 'combattants', canActivate: [AuthGuard], component: CombattantViewComponent },
    { path: 'combattants/:id', canActivate: [AuthGuard], component: SingleCombattantComponent },
    { path: 'edit', canActivate: [AuthGuard], component: EditCombattantComponent },
    { path: 'auth', component: AuthComponent },
    { path: 'users', component: UserListComponent },
    { path: 'new-user', component: NewUserComponent }, 
    { path: '', component: CombattantViewComponent },
    { path: 'not-found', component: FourOhFourComponent },
    { path: '**', redirectTo: '/not-found' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
